import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {

  constructor() {
    this.loadScripts();
  }
 
  loadScripts() {
    const externalScriptArray = [
      '../assets/js/jquery.validate.min.js',
      '../assets/js/additional-methods.min.js',
      '../assets/js/jquery.steps.min.js',
      '../assets/js/dobpicker.js',
      '../assets/js/main.js',

    ];
    for (let i = 0; i < externalScriptArray.length; i++) {
      const scriptTag = document.createElement('script');
      scriptTag.src = externalScriptArray[i];
      scriptTag.type = 'text/javascript';
      scriptTag.async = false;
      scriptTag.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(scriptTag);
    }
  }

  ngOnInit() {
  }

}
