import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartsComponent } from './components/charts/charts.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TablesComponent } from './components/tables/tables.component';
import { ButtonsComponent } from './components/buttons/buttons.component';
import { FormsComponent } from './components/forms/forms.component';
import { BasicFormComponent } from './components/basic-form/basic-form.component';
import { SlidersComponent } from './components/sliders/sliders.component';
import { LoadingComponent } from './components/loading/loading.component';
import { TimeLineComponent } from './components/time-line/time-line.component';
import { OrgchartComponent } from './components/orgchart/orgchart.component';
import { TextEditorComponent } from './components/text-editor/text-editor.component';

const routes: Routes = [
  { path: "dashboard", component: DashboardComponent },
  { path: "charts", component: ChartsComponent },
  { path: "tables", component: TablesComponent },
  { path: "buttons", component: ButtonsComponent },
  { path: "wizard", component: FormsComponent },
  { path: "forms", component: BasicFormComponent },
  { path: "sliders", component: SlidersComponent },
  { path: "loading", component: LoadingComponent },
  { path: "timeline", component: TimeLineComponent },
  { path: "orgchart", component: OrgchartComponent },
  { path: "texteditor", component: TextEditorComponent },
  



];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
