import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChartsComponent } from './components/charts/charts.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TablesComponent } from './components/tables/tables.component';
import { ButtonsComponent } from './components/buttons/buttons.component';
import { FormsComponent } from './components/forms/forms.component';
import { BasicFormComponent } from './components/basic-form/basic-form.component';
import { SlidersComponent } from './components/sliders/sliders.component';
import { LoadingComponent } from './components/loading/loading.component';
import { TimeLineComponent } from './components/time-line/time-line.component';
import { OrgchartComponent } from './components/orgchart/orgchart.component';
import { TextEditorComponent } from './components/text-editor/text-editor.component'; 

@NgModule({
  declarations: [
    AppComponent,
    ChartsComponent,
    DashboardComponent,
    TablesComponent,
    ButtonsComponent,
    FormsComponent,
    BasicFormComponent,
    SlidersComponent,
    LoadingComponent,
    TimeLineComponent,
    OrgchartComponent,
    TextEditorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
