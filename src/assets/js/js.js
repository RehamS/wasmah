$(document).ready(function () {

    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;

    $(".next").click(function () {

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //Add Class Active
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({ opacity: 0 }, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                next_fs.css({ 'opacity': opacity });
            },
            duration: 600
        });
    });

    $(".previous").click(function () {

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();

        //hide the current fieldset with style
        current_fs.animate({ opacity: 0 }, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                previous_fs.css({ 'opacity': opacity });
            },
            duration: 600
        });
    });

    $('.radio-group .radio').click(function () {
        $(this).parent().find('.radio').removeClass('selected');
        $(this).addClass('selected');
    });

    $(".submit").click(function () {
        return false;
    })
    // text editor

    function boldText() {
        var target = document.getElementById("TextArea");
        if (target.style.fontWeight == "bolder") {
            target.style.fontWeight = "normal";
        } else {
            target.style.fontWeight = "bolder";
        }
    }



    function italicText() {
        var target = document.getElementById("TextArea");
        if (target.style.fontStyle == "italic") {
            target.style.fontStyle = "normal";
        } else {
            target.style.fontStyle = "italic";
        }
    }

    function underlineText() {
        var target = document.getElementById("TextArea");
        if (target.style.textDecoration == "underline") {
            target.style.textDecoration = "none";
        } else {
            target.style.textDecoration = "underline";
        }
    }
    function justifyRight() {
        var target = document.getElementById("TextArea");
        if (target.style.textAlign == "justifyLeft") {
            target.style.textAlign = "inherit";
        } else {
            target.style.textAlign = "justifyLeft";
        }
    }
    function justifyRight() {
        var target = document.getElementById("TextArea");
        if (target.style.textAlign == "justifyCenter") {
            target.style.textAlign = "inherit";
        } else {
            target.style.textAlign = "justifyCenter";
        }
    }
    function insertOrderedList() {
        var target = document.getElementById("TextArea");
        if (target.style.insertUnorderedList == "insertUnorderedList") {
            target.style.insertUnorderedList = "inherit";
        } else {
            target.style.insertOrderedList = "insertUnorderedList";
        }
    }
    function insertOrderedList() {
        var target = document.getElementById("TextArea");
        if (target.style.insertOrderedList == "insertOrderedList") {
            target.style.insertOrderedList = "inherit";
        } else {
            target.style.insertOrderedList = "insertOrderedList";
        }
    }
    // table
    var $TABLE = $('#table');
    var $BTN = $('#export-btn');
    var $EXPORT = $('#export');

    $('.table-add').click(function () {
        var $clone = $TABLE.find('tr.hide').clone(true).removeClass('hide table-line');
        $TABLE.find('table').append($clone);
    });

    $('.table-remove').click(function () {
        $(this).parents('tr').detach();
    });

    $('.table-up').click(function () {
        var $row = $(this).parents('tr');
        if ($row.index() === 1) return; // Don't go above the header
        $row.prev().before($row.get(0));
    });

    $('.table-down').click(function () {
        var $row = $(this).parents('tr');
        $row.next().after($row.get(0));
    });

    // A few jQuery helpers for exporting only
    jQuery.fn.pop = [].pop;
    jQuery.fn.shift = [].shift;

    $BTN.click(function () {
        var $rows = $TABLE.find('tr:not(:hidden)');
        var headers = [];
        var data = [];

        // Get the headers (add special header logic here)
        $($rows.shift()).find('th:not(:empty)').each(function () {
            headers.push($(this).text().toLowerCase());
        });

        // Turn all existing rows into a loopable array
        $rows.each(function () {
            var $td = $(this).find('td');
            var h = {};

            // Use the headers from earlier to name our hash keys
            headers.forEach(function (header, i) {
                h[header] = $td.eq(i).text();
            });

            data.push(h);
        });

    });


});
